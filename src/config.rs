extern crate tcod;

use tcod::console::*;
use tcod::map::{Map as FovMap};
use tcod::input::Mouse;

/// MAP
pub const MAP_HEIGHT: i32 = 43;
pub const MAP_WIDTH: i32 = 80;

/// SCREEN
pub const SCREEN_HEIGHT: i32 = 50;
pub const SCREEN_WIDTH: i32 = 80;

pub const PANEL_HEIGHT: i32 = 7;

pub struct Tcod {
    pub root: Root,
    pub con: Offscreen,
    pub panel: Offscreen,
    pub fov: FovMap,
    pub mouse: Mouse,
}
