extern crate tcod;
extern crate rand;

use tcod::colors::{self};
use tcod::map::{Map as FovMap};
use tcod::input::{self, Event};
use tcod::console::*;

mod io;
mod map;
mod object;
mod item;
mod config;

use io::*;
use map::*;
use object::*;
use config::*;

/// General
const LIMIT_FPS: i32 = 20;

fn main() {
    let root = Root::initializer()
        .font("assets/arial10x10.png", FontLayout::Tcod)
        .font_type(FontType::Greyscale)
        .size(SCREEN_WIDTH, SCREEN_HEIGHT)
        .title("Rust/libtcod tutorial")
        .init();
    let mut tcod = Tcod {
        root: root,
        con: Offscreen::new(MAP_WIDTH, MAP_HEIGHT),
        panel: Offscreen::new(SCREEN_WIDTH, PANEL_HEIGHT),
        fov: FovMap::new(MAP_WIDTH, MAP_HEIGHT),
        mouse: Default::default(),
    };

    tcod::system::set_fps(LIMIT_FPS);

    let mut messages = vec![];
    message(&mut messages, "Welcome stranger! Prepare to perish in the Tombs of the Ancient Kings.", colors::RED);

    let player = Object::new_player();
    let mut objects = vec![player; 1];
    let mut map = make_map(&mut objects);
    let mut prev_player_position = (-1, -1);
    for y in 0..MAP_HEIGHT {
        for x in 0..MAP_WIDTH {
            tcod.fov.set(x, y,
                        !map[x as usize][y as usize].block_sight,
                        !map[x as usize][y as usize].blocked);
        }
    }

    let mut inventory: Vec<Object> = vec![];
    let mut key = Default::default();

    while !tcod.root.window_closed() {
        match input::check_for_event(input::MOUSE | input::KEY_PRESS) {
            Some((_, Event::Mouse(m))) => tcod.mouse = m,
            Some((_, Event::Key(k))) => key = k,
            _ => key = Default::default(),
        }

        let fov_recompute = prev_player_position != objects[PLAYER].pos();
        render_all(&mut tcod, &objects, &mut map, &messages, fov_recompute);

        for object in &objects {
            object.clear(&mut tcod.con);
        }

        prev_player_position = objects[PLAYER].pos();
        let player_action = handle_keys(key, &mut map, &mut tcod, &mut objects, &mut inventory, &mut messages);
        if player_action == PlayerAction::Exit {
            break;
        }

        if player_action != PlayerAction::DidntTakeTurn {
            for id in 0..objects.len() {
                if objects[id].ai().is_some() {
                    ai_take_turn(id, &map, &mut objects, &tcod.fov, &mut messages);
                }
            }
        }
    }
}
