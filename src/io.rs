extern crate tcod;

use tcod::input::KeyCode::*;
use tcod::input::{Key, Mouse};
use tcod::map::{Map as FovMap};
use tcod::colors::Color;

use config::Tcod;
use object::*;
use map::Map;
use item::*;

pub type Messages = Vec<(String, Color)>;

const MSG_HEIGHT: usize = 20 as usize - 1;
pub fn message<T: Into<String>>(messages: &mut Messages, message: T, color: Color) {
    if messages.len() == MSG_HEIGHT {
        messages.remove(0);
    }
    messages.push((message.into(), color));
}


#[derive(PartialEq)]
pub enum PlayerAction {
    TookTurn,
    DidntTakeTurn,
    Exit,
}

pub fn get_names_under_mouse(mouse: &Mouse, objects: &[Object], fov_map: &FovMap) -> String {
    let (x, y) = (mouse.cx as i32, mouse.cy as i32);
    let names = objects
        .iter()
        .filter(|o| {
            let (ox, oy) = o.pos();
            (ox, oy) == (x, y) && fov_map.is_in_fov(ox, oy)
        })
        .map(|o| o.name())
        .collect::<Vec<_>>();
    names.join(", ")
}

pub fn handle_keys(key: Key,
                   map: &mut Map,
                   tcod: &mut Tcod,
                   objects: &mut Vec<Object>,
                   inventory: &mut Vec<Object>,
                   messages: &mut Messages) -> PlayerAction {
    use PlayerAction::*;

    let player_alive = objects[PLAYER].alive();
    match (key, player_alive) {
        (Key { code: Up, .. }, true) => {
            player_move_or_attack(0, -1, map, objects, messages);
            TookTurn
        },
        (Key { code: Down, .. }, true) => {
            player_move_or_attack(0, 1, map, objects, messages);
            TookTurn
        },
        (Key { code: Left, .. }, true) => {
            player_move_or_attack(-1, 0, map, objects, messages);
            TookTurn
        },
        (Key { code: Right, .. }, true) => {
            player_move_or_attack(1, 0, map, objects, messages);
            TookTurn
        },
        (Key { code: Enter, ctrl: true, .. }, _) => {
            let fullscreen = tcod.root.is_fullscreen();
            tcod.root.set_fullscreen(!fullscreen);
            DidntTakeTurn
        },
        (Key { code: Escape, .. }, _) => Exit,
        (Key { printable: 'g', .. }, true ) => {
            let item_id = objects.iter().position(|o| {
                o.pos() == objects[PLAYER].pos() && o.get_item().is_some()
            });
            if let Some(item_id) = item_id {
                pick_item_up(item_id, objects, inventory, messages);
            }
            DidntTakeTurn
        },
        (Key { printable: 'i', .. }, true) => {
            let inventory_index = inventory_menu(inventory,
                "Press the key next to an item to use it, or any other to cancel.\n",
                &mut tcod.root);
            if let Some(inventory_index) = inventory_index {
                use_item(inventory_index, inventory, objects, messages, tcod);
            }
            TookTurn
        },
        _ => DidntTakeTurn,
    }
}
