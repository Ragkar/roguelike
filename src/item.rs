use object::{Object, PLAYER, closest_monster};
use io::{message, Messages};

use tcod::colors::{self};
use tcod::console::*;
use config::*;

pub const INVENTORY_SIZE: usize = 26;
pub const INVENTORY_WIDTH: i32 = 50;
const HEAL_AMOUNT: i32 = 4;

const LIGHTNING_RANGE: i32 = 5;
const LIGHTNING_DAMAGE: i32 = 20;

#[derive(Clone)]
pub enum Item {
    Heal,
    Lightning,
}

enum UseResult {
    UsedUp,
    Cancelled,
}

pub fn pick_item_up(object_id: usize,
                objects: &mut Vec<Object>,
                inventory: &mut Vec<Object>,
                messages: &mut Messages)
{
    if inventory.len() > INVENTORY_SIZE {
        message(messages,
                format!("Your inventory is full, you cannot pick up {}.", objects[object_id].name()),
                colors::RED);
    } else {
        let item = objects.swap_remove(object_id);
        message(messages, format!("You picked up a {}!", item.name()), colors::GREEN);
        inventory.push(item);
    }
}

fn menu<T: AsRef<str>>(header: &str, options: & [T], width: i32, root: &mut Root) -> Option<usize> {
    assert!(options.len() <= INVENTORY_SIZE, "Cannot have a menu with more than 26 options.");
    let header_height = root.get_height_rect(0, 0, width, SCREEN_HEIGHT, header);
    let height = options.len() as i32 + header_height;

    let mut window = Offscreen::new(width, height);
    window.set_default_foreground(colors::WHITE);
    window.print_rect_ex(0, 0, width, height, BackgroundFlag::None,
                         TextAlignment::Left, header);

    for (index, option_text) in options.iter().enumerate() {
        let menu_letter = (b'a' + index as u8) as char;
        let text = format!("({}) {}", menu_letter, option_text.as_ref());
        window.print_ex(0, header_height + index as i32, BackgroundFlag::None,
                        TextAlignment::Left, text);
    }
    let x = SCREEN_WIDTH / 2 - width / 2;
    let y = SCREEN_HEIGHT / 2 - height / 2;
    blit(&mut window, (0, 0), (width, height), root, (x, y), 1.0, 0.7);
    root.flush();
    let key = root.wait_for_keypress(true);

    if key.printable.is_alphabetic() {
        let index = key.printable.to_ascii_lowercase() as usize - 'a' as usize;
        if index < options.len() {
            Some(index)
        } else {
            None
        }
    } else {
        None
    }
}

pub fn inventory_menu(inventory: &[Object], header: &str, root: &mut Root) -> Option<usize> {
    let options = if inventory.len() == 0 {
        vec!["Inventory is empty.".into()]
    } else {
        inventory.iter().map(|i| { i.name() }).collect()
    };
    let inventory_index = menu(header, &options, INVENTORY_WIDTH, root);
    if inventory.len() > 0 {
        inventory_index
    } else {
        None
    }
}

fn cast_heal(_inventory_id: usize,
             objects: &mut [Object],
             messages: &mut Messages,
             _tcod: &mut Tcod) -> UseResult {
    if let Some(fighter) = objects[PLAYER].mut_fighter() {
        let (hp, max_hp) = fighter.hp_info();
        if hp == max_hp {
            message(messages, "You are already at full health.", colors::RED);
            UseResult::Cancelled
        } else {
            message(messages, "Your wounds start to feel better!", colors::LIGHT_VIOLET);
            fighter.heal(HEAL_AMOUNT);
            UseResult::UsedUp
        }
    } else {
        UseResult::Cancelled
    }
}

fn cast_lightning(_inventory_id: usize,
                  objects: &mut [Object],
                  messages: &mut Messages,
                  tcod: &mut Tcod) -> UseResult
{
    let monster_id = closest_monster(LIGHTNING_RANGE, objects, tcod);
    if let Some(monster_id) = monster_id {
        message(messages,
                format!("A lightning bolt strikes the {} with a loud thunder! \
                        The damage is {} hit points.",
                        objects[monster_id].name(), LIGHTNING_DAMAGE),
                colors::LIGHT_BLUE);
        objects[monster_id].take_damage(LIGHTNING_DAMAGE, messages);
        UseResult::UsedUp
    } else {
        message(messages, "No enemy is close enough to strike.", colors::RED);
        UseResult::Cancelled
    }
}

pub fn use_item(inventory_id: usize,
                inventory: &mut Vec<Object>,
                objects: &mut [Object],
                messages: &mut Messages,
                tcod: &mut Tcod) {
    if let Some(item) = inventory[inventory_id].get_item() {
        let on_use: fn(usize, &mut [Object], &mut Messages, &mut Tcod)-> UseResult = match item {
            Item::Heal => cast_heal,
            Item::Lightning => cast_lightning,
        };
        match on_use(inventory_id, objects, messages, tcod) {
            UseResult::UsedUp => {
                inventory.remove(inventory_id);
            }
            UseResult::Cancelled => {
                message(messages, "Cancelled", colors::WHITE);
            }
        }
    } else {
        message(messages,
                format!("The {} cannot be used.", inventory[inventory_id].name()),
                colors::WHITE);
    }
}
