extern crate tcod;

use std::cmp;

use tcod::console::*;
use tcod::colors::{self, Color};
use tcod::map::{Map as FovMap};

use map::Map;
use io::{Messages, message};
use item::*;
use config::Tcod;

/// Player
pub const PLAYER: usize = 0;

#[derive(Clone, Copy)]
enum DeathCallback {
    Player,
    Monster,
}

impl DeathCallback {
    fn callback(self, object: &mut Object, messages: &mut Messages) {
        let callback: fn(&mut Object, &mut Messages) = match self {
            DeathCallback::Player => player_death,
            DeathCallback::Monster => monster_death,
        };
        callback(object, messages);
    }
}

fn player_death(player: &mut Object, messages: &mut Messages) {
    message(messages, "You died!", colors::RED);
    player.char = '%';
    player.color = colors::DARK_RED;
}

fn monster_death(monster: &mut Object, messages: &mut Messages) {
    message(messages, format!("A {} is dead!", monster.name), colors::ORANGE);
    monster.char = '%';
    monster.color = colors::DARK_RED;
    monster.blocks = false;
    monster.fighter = None;
    monster.ai = None;
    monster.name = format!("Remains of {}", monster.name);
}

#[derive(Clone, Copy)]
pub struct Fighter {
    max_hp: i32,
    hp: i32,
    defense: i32,
    power: i32,
    on_death: DeathCallback,
}

impl Fighter {
    pub fn hp(&self) -> i32 {
        self.hp
    }

    pub fn hp_info(&self) -> (i32, i32) {
        (self.max_hp, self.hp)
    }

    pub fn heal(&mut self, amount: i32) {
        self.hp = (self.hp + amount) % (self.max_hp + 1);
    }
}

#[derive(Clone)]
pub enum Ai {
    Basic,
    Confused{
        previous_ai: Box<Ai>,
        num_turns: i32,
    },
}

#[derive(Clone)]
pub struct Object {
    x: i32,
    y: i32,
    char: char,
    color: Color,
    name: String,
    blocks: bool,
    alive: bool,
    fighter: Option<Fighter>,
    ai: Option<Ai>,
    item: Option<Item>,
}

impl Object {
    pub fn new(x: i32, y: i32, char: char, name: &str, color: Color, blocks: bool) -> Self {
        Object {
            x: x,
            y: y,
            char: char,
            color: color,
            name: name.into(),
            blocks: blocks,
            alive: false,
            fighter: None,
            ai: None,
            item: None
        }
    }

    pub fn new_player() -> Self {
        let mut player = Object::new(0, 0, '@', "player", colors::WHITE, true);
        player.alive = true;
        player.fighter = Some(Fighter {
            max_hp: 30,
            hp: 30,
            defense: 2,
            power: 5,
            on_death: DeathCallback::Player,
        });
        player
    }

    pub fn new_orc(x: i32, y: i32) -> Self {
        let mut orc = Object::new(x, y, 'o', "orc", colors::DESATURATED_GREEN, true);
        orc.alive = true;
        orc.fighter = Some(Fighter {
            max_hp: 10,
            hp: 10,
            defense: 0,
            power: 3,
            on_death: DeathCallback::Monster,
        });
        orc.ai = Some(Ai::Basic);
        orc
    }

    pub fn new_troll(x: i32, y: i32) -> Self {
        let mut troll = Object::new(x, y, 'T', "troll", colors::DARKER_GREEN, true);
        troll.alive = true;
        troll.fighter = Some(Fighter {
            max_hp: 16,
            hp: 16,
            defense: 1,
            power: 4,
            on_death: DeathCallback::Monster,
        });
        troll.ai = Some(Ai::Basic);
        troll
    }

    pub fn set_item(&mut self, item: Option<Item>) {
        self.item = item;
    }

    pub fn get_item(&self) -> Option<Item> {
        return self.item.clone();
    }

    pub fn pos(&self) -> (i32, i32) {
        (self.x, self.y)
    }

    pub fn set_pos(&mut self, x: i32, y: i32) {
        self.x = x;
        self.y = y;
    }

    pub fn blocking(&self) -> bool {
        self.blocks
    }

    pub fn name(&self) -> String {
        self.name.clone()
    }

    pub fn alive(&self) -> bool {
        self.alive.clone()
    }

    pub fn fighter(&self) -> &Option<Fighter> {
        &self.fighter
    }

    pub fn mut_fighter(&mut self) -> &mut Option<Fighter> {
        &mut self.fighter
    }

    pub fn ai(&self) -> &Option<Ai> {
        &self.ai
    }

    pub fn mut_ai(&mut self) -> &mut Option<Ai> {
        &mut self.ai
    }

    pub fn draw(&self, con: &mut Console) {
        con.set_default_foreground(self.color);
        con.put_char(self.x, self.y, self.char, BackgroundFlag::None);
    }

    pub fn clear(&self, con: &mut Console) {
        con.put_char(self.x, self.y, ' ', BackgroundFlag::None);
    }

    pub fn distance_to(&self, other: &Object) -> f32 {
        let dx = other.x - self.x;
        let dy = other.y  - self.y;
        ((dx.pow(2) + dy.pow(2)) as f32).sqrt()
    }

    pub fn take_damage(&mut self, damage: i32, messages: &mut Messages) {
        if let Some(fighter) = self.fighter.as_mut() {
            if damage > 0 {
                fighter.hp -= damage;
            }
        }
        if let Some(fighter) = self.fighter {
            if fighter.hp <= 0 {
                self.alive = false;
                fighter.on_death.callback(self, messages);
            }
        }
    }

    pub fn attack(&mut self, target: &mut Object, messages: &mut Messages) {
        let damage = self.fighter.map_or(0, |f| f.power)
            - target.fighter.map_or(0, |f| f.defense);
        if damage > 0 {
            message(messages,
                    format!("{} attacks {} for {} hit points.", self.name, target.name, damage),
                    colors::WHITE);
            target.take_damage(damage, messages);
        } else {
            message(messages,
                    format!("{} attacks {} but it has no effect!", self.name, target.name),
                    colors::WHITE);
        }
    }
}

pub fn closest_monster(max_range: i32, objects: &mut [Object], tcod: &Tcod) -> Option<usize> {
    let mut closest_enemy = None;
    let mut closest_dist = (max_range + 1) as f32;

    for (id, object) in objects.iter().enumerate() {
        if (id != PLAYER) && object.fighter().is_some() && object.ai().is_some()
            && tcod.fov.is_in_fov(object.x, object.y)
        {
            let dist = objects[PLAYER].distance_to(object);
            if dist < closest_dist {
                closest_enemy = Some(id);
                closest_dist = dist;
            }
        }
    }
    closest_enemy
}

pub fn move_by(id: usize, dx: i32, dy: i32, map: &Map, objects: &mut [Object]) {
    let (x, y) = objects[id].pos();

    if !is_blocked(x, y + dy, map, objects) && dy != 0 {
        objects[id].set_pos(x, y + dy);
    } else if !is_blocked(x + dx, y, map, objects) && dx != 0 {
        objects[id].set_pos(x + dx, y);
    }
}

pub fn move_towards(id: usize, target_x: i32, target_y: i32, map: &Map, objects: &mut [Object]) {
    let dx = target_x - objects[id].x;
    let dy = target_y - objects[id].y;
    let dx = if dx >= 1 { 1 } else if dx <= -1 { -1 } else { 0 };
    let dy = if dy >= 1 { 1 } else if dy <= -1 { -1 } else { 0 };
    move_by(id, dx, dy, map, objects);
}

pub fn is_blocked(x: i32, y: i32, map: &Map, objects: &[Object]) -> bool {
    if map[x as usize][y as usize].blocked {
        return true;
    }

    objects.iter().any(|object| object.blocks && object.pos() == (x, y))
}

pub fn player_move_or_attack(dx: i32, dy: i32, map: &Map, objects: &mut [Object], messages: &mut Messages) {
    let x = objects[PLAYER].x + dx;
    let y = objects[PLAYER].y + dy;

    let monster_id = objects.iter().position(|object| {
        object.fighter.is_some() && object.pos() == (x, y)
    });
    match monster_id {
        None => move_by(PLAYER, dx, dy, map, objects),
        Some(monster_id) => {
            let (player, monster) = mut_two(PLAYER, monster_id, objects);
            player.attack(monster, messages);
        },
    }
}

pub fn ai_take_turn(monster_id: usize,
                    map: &Map,
                    objects: &mut [Object],
                    fov_map: &FovMap,
                    messages: &mut Messages) {
    if let Some(ai) = objects[monster_id].mut_ai().take() {
        let new_ai = match ai {
            Ai::Basic => ai_basic(monster_id, map, objects, fov_map, messages),
            Ai::Confused{previous_ai, num_turns} => ai_confused(
                monster_id, map, objects, messages, previous_ai, num_turns)
        };
        objects[monster_id].ai = Some(new_ai);
    }
}

pub fn ai_basic(monster_id: usize,
                    map: &Map,
                    objects: &mut [Object],
                    fov_map: &FovMap,
                    messages: &mut Messages) -> Ai {
    let (monster_x, monster_y) = objects[monster_id].pos();
    if fov_map.is_in_fov(monster_x, monster_y) {
        let distance = objects[monster_id].distance_to(&objects[PLAYER]);
        if distance > 1.0 {
            let (player_x, player_y) = objects[PLAYER].pos();
            move_towards(monster_id, player_x, player_y, map, objects);
        } else if (&objects[PLAYER]).fighter().map_or(false, |f| f.hp() > 0) {
            let (monster, player) = mut_two(monster_id, PLAYER, objects);
            monster.attack(player, messages);
        }
    }
    Ai::Basic
}

pub fn ai_confused(monster_id: usize,
                  map: &Map,
                  objects: &mut [Object],
                  messages: &mut Messages,
                  previous_ai: Box<Ai>,
                  num_turns: i32) -> Ai {
    Ai::Basic
}

/*-- Misc -------------------------------------------------------------------*/

fn mut_two<T>(i1: usize, i2: usize, array: &mut [T]) -> (&mut T, &mut T) {
    assert!(i1 != i2);
    let i_max = cmp::max(i1, i2);
    let (slice1, slice2) = array.split_at_mut(i_max);
    if i1 < i2 {
        (&mut slice1[i1], &mut slice2[0])
    } else {
        (&mut slice2[0], &mut slice1[i2])
    }
}
